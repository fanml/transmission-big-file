package com.fanml.service;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * @title: NettyInitializerFilter
 * @Author fanml
 * @describe: //TODO
 * @Date: 2021/7/4 17:21
 * @Version 1.0
 */
public class NettyInitializerFilter<T> extends ChannelInitializer<SocketChannel> {

    private T t;

    public NettyInitializerFilter(T t) {
        this.t = t;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast(new MsgEncoder());
        pipeline.addLast(new MsgDecoder());
        pipeline.addLast((ChannelHandler) t);
    }
}