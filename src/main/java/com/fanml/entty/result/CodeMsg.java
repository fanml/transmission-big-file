package com.fanml.entty.result;

public class CodeMsg {
    private int retCode;
    private String message;
    // 按照模块定义CodeMsg
    // 通用异常
    public static CodeMsg SUCCESS = new CodeMsg(0, "success");
    public static CodeMsg SERVER_EXCEPTION = new CodeMsg(500100, "服务器内部错误");
    public static CodeMsg PARAMETER_ISNULL = new CodeMsg(500101, "输入参数为空");
    // 业务异常
    public static CodeMsg USER_NOT_EXSIST = new CodeMsg(500101, "帐号不存在");
    public static CodeMsg USER_EXSIST = new CodeMsg(500102, "帐号已存在");
    public static CodeMsg USER_PASSWORD_ERROR = new CodeMsg(500103, "帐号或密码错误");
    public static CodeMsg COMMON_TOKEN_INVALID = new CodeMsg(500104, "token已失效");
    public static CodeMsg COMMON_TOKEN_ERROR = new CodeMsg(500105, "缺失token参数");
    public static CodeMsg COMMON_TOKEN_EXPIRE = new CodeMsg(500106, "token即将过期");

    public static CodeMsg ONLINE_USER_OVER = new CodeMsg(500107, "在线用户数超出允许登录的最大用户限制");
    public static CodeMsg SESSION_NOT_EXSIST = new CodeMsg(500108, "不存在离线session数据");

    public static CodeMsg BODY_NOT_MATCH = new CodeMsg(500200, "请求的数据格式不符");
    public static CodeMsg NOT_FIND_DATA = new CodeMsg(500201, "查找不到对应数据");
    public static CodeMsg NOT_RESUBMIT = new CodeMsg(500202, "请勿重复提交数据");
    public static CodeMsg IMAGE_ERROR = new CodeMsg(400203, "底图不符合规范，请上传符合JPEG规范的图片文件");
    public static CodeMsg DELETE_TABLE_ERROR = new CodeMsg(400204, "表格删除失败，请先解绑字段关系");


    private CodeMsg(int retCode, String message) {
        this.retCode = retCode;
        this.message = message;
    }

    public int getRetCode() {
        return retCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
