package com.fanml.entty.netty;

import com.fanml.entty.constant.ConstantValue;
import com.fanml.entty.enums.MsgTypeEnum;
import io.netty.util.internal.StringUtil;

import java.util.UUID;
/**
 * @title: Message
 * @Author fanml
 * @describe: //TODO
 * @Date: 2021/7/4 16:55
 * @Version 1.0
 */
public class Message {
    /**
     * 消息头
     */
    private int msgHead = ConstantValue.MSG_HEAD;
    /**
     * 会话id
     */
    private String sessionId;
    /**
     * 消息类型
     */
    private MsgTypeEnum msgTypeEnum;
    /**
     * 消息长度
     */
    private int msgLength;
    /**
     * 消息体
     */
    private byte[] msgBody;

    public Message(String sessionId, MsgTypeEnum msgTypeEnum, byte[] msgBody) {
        if (StringUtil.isNullOrEmpty(sessionId)) {
            sessionId = UUID.randomUUID().toString().replaceAll("-", "");
        }
        this.sessionId = sessionId;
        this.msgTypeEnum = msgTypeEnum;
        this.msgBody = msgBody;
        this.msgLength = msgBody.length;
    }

    public Message() {
    }

    public String getSessionId() {
        return sessionId;
    }

    public MsgTypeEnum getMsgTypeEnum() {
        return msgTypeEnum;
    }

    public int getMsgLength() {
        return msgLength;
    }

    public byte[] getMsgBody() {
        return msgBody;
    }

    public void setMsgHead(int msgHead) {
        this.msgHead = msgHead;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setMsgTypeEnum(MsgTypeEnum msgTypeEnum) {
        this.msgTypeEnum = msgTypeEnum;
    }

    public void setMsgLength(int msgLength) {
        this.msgLength = msgLength;
    }

    public void setMsgBody(byte[] msgBody) {
        this.msgBody = msgBody;
        this.msgLength = msgBody == null ? 0 : msgBody.length;
    }
}
