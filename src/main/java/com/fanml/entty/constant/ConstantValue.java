package com.fanml.entty.constant;
/**
 * @title: ConstantValue
 * @Author fanml
 * @describe: //TODO
 * @Date: 2021/7/4 17:13
 * @Version 1.0
 */
public class ConstantValue {

    public static final int MSG_HEAD = 0x77;

    public static final long BLOCK_SIZE = 25 * 1024 * 1024;//25MB

    public static final String UPLOAD_FILE_PATH = "D:\\nettyFileServer\\";

    public static final String UPLOAD_FILE_BLOCK_PATH = "D:\\nettyFileServerBlock\\";

    public static final String DOWNLOAD_FILE_PATH = "D:\\nettyFileClient\\";

    public static final String DOWNLOAD_FILE_BLOCK_PATH = "D:\\nettyFileClientBlock\\";

}
