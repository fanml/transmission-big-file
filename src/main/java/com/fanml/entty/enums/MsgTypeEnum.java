package com.fanml.entty.enums;
/**
 * @title: MsgTypeEnum
 * @Author fanml
 * @describe: //TODO
 * @Date: 2021/7/4 17:00
 * @Version 1.0
 */
public enum MsgTypeEnum {
    REQUEST(1), RESPONSE(2), PING(3), PONG(4), EMPTY(5),DOWNlOAD(6),UPLOAD(7);
    private int type;
    MsgTypeEnum(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }
    public static MsgTypeEnum get(int type) {
        for (MsgTypeEnum value : values()) {
            if (value.type == type) {
                return value;
            }
        }
        throw new RuntimeException("unsupported type: " + type);
    }
}
