package com.fanml.entty;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;

/**
 * @title: FileInfo
 * @Author fanml
 * @describe: //TODO
 * @Date: 2021/7/4 16:57
 * @Version 1.0
 */
public class FileInfo implements Serializable {

    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件名MD5
     */
    private String md5FileName;
    /**
     * 文件长度
     */
    private long fileLength;
    /**
     * 文件当前块号
     */
    private int blockIndex;
    /**
     * 文件总块数
     */
    private int blockCount;
    /**
     * 文件块内容
     */
    private byte[] content;
    /**
     * 文件块MD5
     */
    private String md5Content;

    public FileInfo(String fileName, long fileLength, int blockCount, int blockIndex, byte[] content) {
        this.fileName = fileName;
        this.fileLength = fileLength;
        this.blockCount = blockCount;
        this.blockIndex = blockIndex;
        this.content = content;
        this.md5Content = content == null ? null : DigestUtils.md5Hex(content);
        this.md5FileName = DigestUtils.md5Hex(fileName + fileLength);

    }

    public String getFileName() {
        return fileName;
    }

    public String getMd5FileName() {
        return md5FileName;
    }

    public long getFileLength() {
        return fileLength;
    }

    public int getBlockIndex() {
        return blockIndex;
    }

    public int getBlockCount() {
        return blockCount;
    }

    public void setBlockCount(int blockCount) {
        this.blockCount = blockCount;
    }

    public byte[] getContent() {
        return content;
    }

    public String getMd5Content() {
        return md5Content;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setMd5FileName(String md5FileName) {
        this.md5FileName = md5FileName;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public void setBlockIndex(int blockIndex) {
        this.blockIndex = blockIndex;
    }

    public void setContent(byte[] content) {
        this.content = content;
        if (content != null)
            this.setMd5Content(DigestUtils.md5Hex(content));
    }

    public void setMd5Content(String md5Content) {
        this.md5Content = md5Content;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "fileName='" + fileName + '\'' +
                ", md5FileName='" + md5FileName + '\'' +
                ", fileLength=" + fileLength +
                ", blockIndex=" + blockIndex +
                ", blockCount=" + blockCount +
                ", md5Content='" + md5Content + '\'' +
                '}';
    }

}
